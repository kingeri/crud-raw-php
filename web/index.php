<?php

ini_set('include_path', ini_get('include_path') . ':' . dirname(__FILE__) . '/../');

include_once('core/router.php');
include_once('core/template.php');

$template = new Template();

$router = new Router();
$content = $router->dispatch($_SERVER['REQUEST_URI']);


echo $template->render('layouts/main', [ 'content' => $content ]);


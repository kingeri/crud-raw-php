<?php

include_once('core/controller.php');

class Employees_Controller extends Controller {

  function create() {
    $employee = $_POST['employee'];
    if ($employee['first_name'] && $employee['last_name']) {
      $this->database->insert('employees', $employee);
    }
    $this->redirectTo('/');
  }

  function add() {
    return $this->template->render('employees/new');
  }

}

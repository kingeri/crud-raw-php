<?php

include_once('core/controller.php');

class Welcome_Controller extends Controller {

  function index() {
    $employees = $this->database->all('employees');
    return $this->template->render('welcome/index', [ 'employees' => $employees ]);
  }

}

<h1>Add Employee</h1>

<form action="/employees" method="POST">
  <div class="form-group">
    <label for="first-name">First Name</label>
    <input type="text" class="form-control" id="first-name"
           name="employee[first_name]"
           aria-describedby="firstName" placeholder="First Name...">
  </div>
  <div class="form-group">
    <label for="last-name">Last Name</label>
    <input type="text" class="form-control" id="last-name"
           name="employee[last_name]"
           aria-describedby="lastName" placeholder="Last Name...">
  </div>
  <button type="submit" class="btn btn-success">Create Employee</button>
</form>

<h1>Employees</h1>

<table class="table table-bordered">
  <thead>
    <tr>
      <th>First Name</th>
      <th>Last Name</th>
  </thead>
  <tbody>
  <?php foreach($employees as $employee): ?>
    <tr>
      <td><?= $employee['first_name'] ?></td>
      <td><?= $employee['last_name'] ?></td>
    </tr>
  <?php endforeach; ?>
  </tbody>
</table>

<a href="/employees/new" class="btn btn-primary">Add New</a>


<?php

class Database {

  var $DATABASE_URL;
  var $dbh;

  function __construct()
  {
    $this->DATABASE_URL = getenv('CLEARDB_DATABASE_URL');
  }
  
  function connect() {

    $parsed = parse_url($this->DATABASE_URL);

    $host     = $parsed['host'];
    $dbName   = trim($parsed['path'], '/');
    $user     = $parsed['user'];
    $password = $parsed['pass'];

    $dsn = "mysql:dbname=$dbName;host=$host";
    try {
        $this->dbh = new PDO($dsn, $user, $password);
    } catch (PDOException $e) {
        echo 'Connection failed: ' . $e->getMessage();
    }
  }

  function insert($table, $rec) {

      if (!$this->dbh) {
        $this->connect();
      }

      $fields = array_keys($rec);
      $values = array_values($rec);

      $placeholders = array_map(function() { return '?'; }, $values);
      $sql = "INSERT INTO $table(" . implode(', ', $fields) . ") " .
             " VALUES(" . implode(', ', $placeholders) . ")";

      $stmt = $this->dbh->prepare($sql);
      foreach ($values as $i => $v) {
        $stmt->bindParam($i + 1, $values[$i]);
      }
      $stmt->execute();
      $stmt = null;
  }

  function all($table, $options = []) {
    $result = [];

    if (!$this->dbh) {
      $this->connect();
    }

    if ($this->dbh) {
      $sql = "SELECT * FROM $table";
      if (isset($options['order'])) {
        $sql .= " ORDER BY {$options['order']} ";
      }
      $stmt = $this->dbh->prepare($sql);
      $stmt->execute();
      while ($row = $stmt->fetch(PDO::FETCH_ASSOC, PDO::FETCH_ORI_NEXT)) {
        $result[] = $row;
      }
      $stmt = null;
    }
    return $result;
  }

}

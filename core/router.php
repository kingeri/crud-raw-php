<?php

class Router {

  function dispatch($uri) {

    $result = '';
    if ($uri == '/') {
      include_once("controllers/welcome.php");
      $controller = new Welcome_Controller();
      $result = $controller->index();
    } else {
      if (strstr($uri, '/employees')) {
        include_once("controllers/employees.php");
        $controller = new Employees_Controller();
        if ($_POST) {
          $result = $controller->create();
        } else {
          $result = $controller->add();
        }
      }
    }
    return $result;
  }
}

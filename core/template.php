<?php

define('VIEWS_DIR', dirname(__FILE__) . '/../views');

class Template {

  function render($view, $data = []) {
    extract($data);
    ob_start();
    require(VIEWS_DIR . '/' . $view . '.php');
    $content = ob_get_contents();
    ob_end_clean();
    return $content;
  }

}

<?php

include_once('core/database.php');
include_once('core/template.php');

class Controller {

  var $template;

  function __construct() {
    $this->template = new Template();  
    $this->database = new Database();  
  }

  function redirectTo($path) {
    header("Location: $path");
    exit;
  }

}
